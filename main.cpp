#include <iostream>
#include "list.h"

int main()
{
	/*
		How it works:
			1. Check if the list is empty or not.
			2. Add elements
			3. Remove elements, one in the front and one in the back.
			4. Check if there's a number in the list, if there is, remove it.
	*/
	List theList;
	int temporary;
	std::cout << "Start: is the list empty?: ";
	if(theList.isEmpty())
	{
		std::cout << "Yes" << std::endl;	
	}
	else
	{
		std::cout << "No" << std::endl;	
	}
	std::cout << "Add 13, 14, 15 respectively to the head of the list." << std::endl;
	theList.headPush(13);
	theList.headPush(14);
	theList.headPush(15);
	std::cout << "Current list: ";
	theList.showAll();
	std::cout << std::endl << std::endl << "Add 12, 11, 10 respectively to the end of the list." << std::endl;
	theList.tailPush(12);
	theList.tailPush(11);
	theList.tailPush(10);
	std::cout << "Current list: ";
	theList.showAll();
	std::cout << std::endl << std::endl << "Take out elements from the head and the end of the list, one each." << std::endl;
	std::cout << "Removed element: ";
	temporary = theList.headPop();
	std::cout << temporary << " ";
	temporary = theList.tailPop();
	std::cout << temporary << std::endl;
	std::cout << "Current list: ";
	theList.showAll();
	std::cout << std::endl << std::endl << "Is there number 13 in the list?: ";
	if(theList.isInList(13))
	{
		std::cout << "Yes" << std::endl;
		std::cout << "Remove 13 from the list." << std::endl;
		theList.deleteNode(13);
	}
	else
	{
		std::cout << "No" << std::endl;	
	}
	std::cout << "Current list: ";
	theList.showAll();
	std::cout << std::endl << std::endl << "End of demonstration";
	return 0;
}
